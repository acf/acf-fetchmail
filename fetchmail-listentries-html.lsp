<% local data, viewlibrary, page_info, session = ...
htmlviewfunctions = require("htmlviewfunctions")
html = require("acf.html")
%>

<script type="text/javascript">
	if (typeof jQuery == 'undefined') {
		document.write('<script type="text/javascript" src="<%= html.html_escape(page_info.wwwprefix) %>/js/jquery-latest.js"><\/script>');
	}
</script>

<script type="text/javascript">
	if (typeof $.tablesorter == 'undefined') {
		document.write('<script type="text/javascript" src="<%= html.html_escape(page_info.wwwprefix) %>/js/jquery.tablesorter.js"><\/script>');
	}
</script>

<script type="text/javascript">
	$(document).ready(function() {
		$("#list").tablesorter({headers: {0:{sorter: false}}, widgets: ['zebra']});
	});
</script>

<% htmlviewfunctions.displaycommandresults({"editentry", "deleteentry", "createentry"}, session) %>

<% local header_level = htmlviewfunctions.displaysectionstart(data, page_info) %>
<table id="list" class="tablesorter"><thead>
	<tr>
		<th>Action</th>
		<th>Remote Host</th>
		<th>Enabled</th>
		<th>Method</th>
		<th>Remote Mailbox / Domain</th>
	</tr>
</thead><tbody>
<% local redir = cfe({ type="hidden", value=page_info.orig_action }) %>
<% for i,entry in ipairs(data.value) do %>
	<tr>
		<td>
			<%
			local remotehost = cfe({ type="hidden", value=entry.remotehost })
			local entrymethod = cfe({ type="hidden", value=entry.method })
			local remotemailbox = cfe({ type="hidden", value=entry.remotemailbox })
			local localdomain = cfe({ type="hidden", value=entry.localdomain })
			htmlviewfunctions.displayitem(cfe({type="link", value={remotehost=remotehost, method=entrymethod, remotemailbox=remotemailbox, localdomain=localdomain, redir=redir}, label="", option="Edit", action="editentry"}), page_info, -1)
			htmlviewfunctions.displayitem(cfe({type="form", value={remotehost=remotehost, method=entrymethod, remotemailbox=remotemailbox, localdomain=localdomain}, label="", option="Delete", action="deleteentry"}), page_info, -1)
			%>
		</td>
		<td><%= html.html_escape(entry.remotehost) %></td>
		<td><%= html.html_escape(tostring(entry.enabled)) %></td>
		<td><%= html.html_escape(entry.method) %></td>
		<td><% if entry.localdomain and entry.localdomain ~= "" then io.write(html.html_escape(entry.localdomain)) else io.write(html.html_escape(entry.remotemailbox)) end %></td>
	</tr>
<% end %>
</tbody></table>

<% htmlviewfunctions.displayitem(cfe({type="link", value={redir=redir}, label="Add new entry", option="New", action="createentry"}), page_info, 0) %>

<% htmlviewfunctions.displaysectionend(header_level) %>
