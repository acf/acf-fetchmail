local mymodule = {}

mymodule.default_action = "status"

function mymodule.status(self)
	return self.model.getstatus()
end

function mymodule.startstop(self)
	return self.handle_form(self, self.model.get_startstop, self.model.startstop_service, self.clientdata)
end

function mymodule.editconfig(self)
	return self.handle_form(self, self.model.getconfig, self.model.updateconfig, self.clientdata, "Save", "Edit Config", "Configuration Set")
end

function mymodule.expert(self)
	return self.handle_form(self, self.model.get_filedetails, self.model.update_filecontent, self.clientdata, "Save", "Edit Config", "Configuration Set")
end

function mymodule.listentries(self)
	return self.model.readentries()
end

function mymodule.editentry(self)
	return self.handle_form(self, function() return self.model.readentry(self.clientdata.remotehost, self.clientdata.method, self.clientdata.remotemailbox, self.clientdata.localdomain) end, self.model.updateentry, self.clientdata, "Save", "Edit Entry", "Entry Saved")
end

function mymodule.createentry(self)
	return self.handle_form(self, function() return self.model.readentry() end, self.model.createentry, self.clientdata, "Create", "Create Entry", "Entry Created")
end

function mymodule.deleteentry(self)
	return self.handle_form(self, self.model.get_deleteentry, self.model.deleteentry, self.clientdata, "Delete", "Delete Entry", "Entry Deleted")
end

return mymodule
