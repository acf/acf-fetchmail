<% local view, viewlibrary, page_info, session = ... %>
<% htmlviewfunctions = require("htmlviewfunctions") %>
<% html = require("acf.html") %>

<% htmlviewfunctions.displaycommandresults({"editconfig", "editentry", "deleteentry", "createentry"}, session) %>

<% if viewlibrary and viewlibrary.dispatch_component then
	viewlibrary.dispatch_component("status")
end %>

<%
local redir = cfe({ type="hidden", value=page_info.orig_action })
local header_level = htmlviewfunctions.displaysectionstart(cfe({ label="Global Settings" }), page_info)
htmlviewfunctions.displayitem(cfe({type="link", value={redir=redir}, label="Edit global settings", option="Edit", action="editconfig"}), page_info, 0)
htmlviewfunctions.displaysectionend(header_level)
%>

<% if viewlibrary and viewlibrary.dispatch_component then
	viewlibrary.dispatch_component("listentries")
end %>
